package causeandeffect.devices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import causeandeffect.util.Point;

public class TomTomTest {
	
	TomTom objectUnderTest;
	
	@Before
	public void setup(){
		objectUnderTest = new TomTom();
	}

	@Test
	public void driveTo(){
		Point desiredDestination = new Point(1,2);
		objectUnderTest.driveTo(desiredDestination);
		assertTrue(objectUnderTest.route.size()==1);
		assertTrue(objectUnderTest.route.peek().equals(desiredDestination));
	}
	
	@Test
	public void driveRoute(){
		List<Point> routePlan = new ArrayList<Point>();
		routePlan.add(new Point(1,2));
		routePlan.add(new Point(1,3));
		routePlan.add(new Point(1,4));
		routePlan.add(new Point(1,5));
		objectUnderTest.driveRoute(routePlan);
		for (int i = 0; i < routePlan.size(); i++) {
			Point nextInPlan = routePlan.get(i);
			Point nextCurrentDestination = objectUnderTest.route.pop();
			assertEquals(nextInPlan,nextCurrentDestination);
		}
	}
}
