package causeandeffect.hulls;

import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import causeandeffect.debug.DefaultDebug;
import causeandeffect.debug.NullDebug;
import causeandeffect.strategy.Strategy;

public abstract class StrategyHull extends BasicHull {
	
	Map<String,Strategy> strategies = new HashMap<String, Strategy>();
	DefaultDebug debug = new NullDebug();

	public void setDebug(DefaultDebug debugStrategy){
		debug=debugStrategy;
		debug.setTarget(this);
		addStrategy(debug);
	}

	@Override
	public void loop() {
		for (Strategy strategy : strategies.values()) {
			strategy.update();
		}
	}

	@Override
	public void initialise() {
		for (Strategy strategy : strategies.values()) {
			strategy.init();
		}
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		for (Strategy strategy : strategies.values()) {
			strategy.onScannedRobot(event);
		}
	}

	@Override
	public void onHitByBullet(HitByBulletEvent event) {
		for (Strategy strategy : strategies.values()) {
			strategy.onHitByBullet(event);
		}
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		for (Strategy strategy : strategies.values()) {
			strategy.onHitRobot(event);
		}
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		for (Strategy strategy : strategies.values()) {
			strategy.onHitWall(event);
		}
	}

	@Override
	public void onPaint(Graphics2D g) {
		debug.onPaint(g);
	}
	
	
	
	
	
	@Override
	public void onRobotDeath(RobotDeathEvent event) {
		for (Strategy strategy : strategies.values()) {
			strategy.onRobotDeath(event);
		}
		super.onRobotDeath(event);
	}

	@Override
	public void onRoundEnded(RoundEndedEvent event) {
		for (Strategy strategy : strategies.values()) {
			strategy.onRoundEnded(event);
		}
		super.onRoundEnded(event);
	}

	public void addStrategy(Strategy strategy){
		strategies.put(strategy.getClass().getName(), strategy);
		strategy.setTarget(this);
	}
	
	public void removeStrategy(Class<?> clazz){
		strategies.remove(clazz.getName());
	}

}
