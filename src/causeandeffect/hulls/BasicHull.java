package causeandeffect.hulls;

import causeandeffect.threadmanagement.ThreadManager;
import robocode.AdvancedRobot;
import robocode.DeathEvent;
import robocode.WinEvent;

public abstract class BasicHull extends AdvancedRobot {

	@Override
	public void run() {
		initialise();
		while(true){
			loop();
		}
	}

	public abstract void loop();

	public abstract void initialise();

	@Override
	public void onDeath(DeathEvent event) {
		ThreadManager.stop();
		super.onDeath(event);
	}

	@Override
	public void onWin(WinEvent event) {
		ThreadManager.stop();
		super.onWin(event);
	}
	
	
	
	
	
	

}
