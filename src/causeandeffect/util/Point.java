package causeandeffect.util;

public class Point {
	
	public double x = 0;
	public double y = 0;
	
	public Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Point() {
	}

}
