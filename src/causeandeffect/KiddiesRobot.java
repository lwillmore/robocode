package causeandeffect;

import robocode.Robot;

public abstract class KiddiesRobot extends Robot {
	
	
	@Override
	public void run() {
		while(true){			
			this.ahead(100);
			this.turnRight(90);
		}
	}

	public abstract void doThis();

}
