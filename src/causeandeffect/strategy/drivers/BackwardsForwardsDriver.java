package causeandeffect.strategy.drivers;

import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import causeandeffect.strategy.BasicStrategy;

public class BackwardsForwardsDriver extends BasicStrategy {
	
	int forwardBack = 1;

	@Override
	public void update() {
		robot.setAhead(60 * forwardBack);
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		forwardBack*=-1;
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		forwardBack*=-1;
	}
	

}
