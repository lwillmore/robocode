package causeandeffect.strategy;

import java.awt.Graphics2D;

import causeandeffect.hulls.StrategyHull;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;

public class NullStrategy implements Strategy {

	@Override
	public void update() {
		
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		
	}

	@Override
	public void onHitByBullet(HitByBulletEvent event) {
		
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		
	}

	@Override
	public void setTarget(StrategyHull strategyHull) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRobotDeath(RobotDeathEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRoundEnded(RoundEndedEvent event) {
		// TODO Auto-generated method stub
		
	}

}
