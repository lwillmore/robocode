package causeandeffect.strategy.scanners;

import robocode.ScannedRobotEvent;
import causeandeffect.strategy.BasicStrategy;

public class RotateScanner extends BasicStrategy {
	
	
	@Override
	public void init() {
		robot.turnRadarRightRadians(Double.POSITIVE_INFINITY);
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		
	}
	
	

}
