package causeandeffect.strategy.gunners;

import causeandeffect.strategy.BasicStrategy;

public abstract class BasicGunner extends BasicStrategy implements Gunner {

	@Override
	public void update() {
		double fire = getFirePower();
		if (fire > 0 && onTarget()) {
			robot.fire(fire);
		}
		else{
			robot.execute();
		}
	}
}
