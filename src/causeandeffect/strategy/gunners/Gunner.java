package causeandeffect.strategy.gunners;


public interface Gunner {	

	public abstract double getFirePower();
	
	public abstract boolean onTarget();
	

}
