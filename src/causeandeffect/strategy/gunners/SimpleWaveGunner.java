package causeandeffect.strategy.gunners;

import robocode.ScannedRobotEvent;
import causeandeffect.machinelearning.OurBulletWave;
import causeandeffect.machinelearning.StupidEnemyPositionPredictor;
import causeandeffect.strategy.BasicStrategy;
import causeandeffect.util.Point;

public class SimpleWaveGunner extends BasicStrategy{
	
	int max_tick_search = 25;

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		StupidEnemyPositionPredictor epredictor = new StupidEnemyPositionPredictor(e);
		OurBulletWave obulletw = new OurBulletWave(1.0);
		double shootBearing = getShootBearing(epredictor,obulletw);
	}

	private double getShootBearing(StupidEnemyPositionPredictor epredictor,
			OurBulletWave obulletw) {
		for (int tick = 0; tick < max_tick_search; tick++) {
			Point enemyPosition = epredictor.getPredictedPosition(tick);
			Point bulletPosition = obulletw.getPredictedPosition(tick);
			int enemyDistance = getDistanceToMe(enemyPosition);
			int bulletDistance = getDistanceToMe(bulletPosition);
			if(bulletDistance>enemyDistance){
				return getBearingTo(enemyPosition);
			}
			
		}
		return 700;
	}

	private double getBearingTo(Point enemyPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	private int getDistanceToMe(Point enemyPosition) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}
