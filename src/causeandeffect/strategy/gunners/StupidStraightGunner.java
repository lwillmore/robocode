package causeandeffect.strategy.gunners;

import robocode.Rules;
import robocode.ScannedRobotEvent;

public class StupidStraightGunner extends BasicGunner {
	
	double maxRangeToShoot = 200;
	double maxPowerDistance = 80;
	double targetDistance= maxRangeToShoot+1.0;
	double onTargetThreshold = 5;
	double turretAdjust = onTargetThreshold+1;
	double powerStep;
	long lastScan;
	

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		lastScan=System.currentTimeMillis();
		targetDistance=e.getDistance();
		double absoluteBearing = robot.getHeadingRadians() + e.getBearingRadians();
		turretAdjust = absoluteBearing - 
		        robot.getGunHeadingRadians();
		robot.setTurnGunRightRadians(
		    robocode.util.Utils.normalRelativeAngle(turretAdjust));
	}

	@Override
	public double getFirePower() {
		double dif = (targetDistance-maxPowerDistance)*powerStep;
		return (Rules.MAX_BULLET_POWER-dif);
	}

	@Override
	public void init() {
		powerStep = Rules.MAX_BULLET_POWER/(maxRangeToShoot-maxPowerDistance);
	}

	@Override
	public boolean onTarget() {
		return (Math.abs(turretAdjust)<onTargetThreshold && recentlyScanned());
	}

	private boolean recentlyScanned() {
		long now = System.currentTimeMillis();
		return (now-lastScan<200);
	}

	public void setMaxRangeToShoot(double maxRangeToShoot) {
		this.maxRangeToShoot = maxRangeToShoot;
	}

	public void setMaxPowerDistance(double maxPowerDistance) {
		this.maxPowerDistance = maxPowerDistance;
	}
	
	
	
	

}
