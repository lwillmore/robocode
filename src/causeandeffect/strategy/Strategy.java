package causeandeffect.strategy;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import causeandeffect.hulls.StrategyHull;

public interface Strategy {

	public void update();

	public void init();

	public void onScannedRobot(ScannedRobotEvent e);

	public void onHitByBullet(HitByBulletEvent event);

	public void onHitRobot(HitRobotEvent event);

	public void onHitWall(HitWallEvent event);

	void setTarget(StrategyHull strategyHull);

	public void onRobotDeath(RobotDeathEvent event);

	public void onRoundEnded(RoundEndedEvent event);

}
