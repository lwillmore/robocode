package causeandeffect.devices;

public interface TargetSelector {
	
	public String getBestTargetName();

}
