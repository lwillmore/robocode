package causeandeffect.devices;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import robocode.ScannedRobotEvent;

import causeandeffect.strategy.BasicStrategy;

public class ScannedRobotMemory extends BasicStrategy{
	
	private Map<String, Queue<ScannedRobotData>> memory = new HashMap<String,Queue<ScannedRobotData>>();
	private int memoryDepth = 10;
	
	
	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		ScannedRobotData d = new ScannedRobotData(robot.getTime(),e);
		if(memory.containsKey(e.getName())){
			Queue<ScannedRobotData> q = memory.get(e.getName());
			q.offer(d);
			if(q.size()>memoryDepth){
				q.poll();
			}
		}
		else{
			Queue<ScannedRobotData> q= new LinkedList<ScannedRobotData>();
			q.offer(d);
			memory.put(e.getName(), q);
		}
			
	}

}
