package causeandeffect.devices;

import java.util.List;
import java.util.Stack;

import causeandeffect.strategy.BasicStrategy;
import causeandeffect.util.Point;

public class TomTom extends BasicStrategy {

	double arriveThreshold = 1;

	Stack<Point> route = new Stack<Point>();
	private Point currentDestination;

	public void driveTo(Point destination) {
		route.clear();
		route.push(destination);
	}

	public void driveRoute(List<Point> route) {
		// TODO: Optimisation can happen here I think - use data struct that has
		// efficient reverse iteration
		route.clear();
		for (int i = route.size(); i > 0; i--) {
			this.route.push(route.get(i));
		}
	}

	@Override
	public void update() {
		if (thereIsSomewhereToGo()) {
			getThere();
		}
	}

	private void getThere() {
		// TODO Auto-generated method stub

	}

	private boolean thereIsSomewhereToGo() {
		while ((currentDestination == null || haveArrivedAtCurrentDestination())
				&& route.size() > 0) {
			currentDestination = route.pop();
		}
		return currentDestination != null;
	}

	private boolean haveArrivedAtCurrentDestination() {
		double difX = Math.abs(robot.getX() - currentDestination.x);
		double difY = Math.abs(robot.getY() - currentDestination.y);
		return (difX < arriveThreshold && difY < arriveThreshold);
	}

}
