package causeandeffect.devices;

import robocode.ScannedRobotEvent;

public class ScannedRobotData {
	
	
	private String name;
	private long createdOn;
	private ScannedRobotEvent event;
	
	public ScannedRobotData(long createTime, ScannedRobotEvent event) {
		createdOn = createTime;
		this.name = event.getName();
		this.event = event;
	}

	public boolean isOlderThan(int milliseconds) {
		return createdOn+milliseconds < System.currentTimeMillis();
	}

	public String getName() {
		return name;
	}

	public ScannedRobotEvent getEvent() {
		return event;
	}

	public long getCreatedOn() {
		return createdOn;
	}
	
	
	
	

}
