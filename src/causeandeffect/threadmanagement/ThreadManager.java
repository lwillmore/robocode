package causeandeffect.threadmanagement;

import java.util.ArrayList;
import java.util.List;

public class ThreadManager {
	
	public static int THREAD_LIMIT = 5;
	private static List<StopRunnable> threads = new ArrayList<StopRunnable>();
	
	public static void addRunnable(StopRunnable runnable) throws TooManyThreadsException{
		if(threads.size()<THREAD_LIMIT){
			Thread t = new Thread(runnable);
			runnable.setThread(t);
			t.start();
			threads.add(runnable);
		}
		else{
			throw new TooManyThreadsException();
		}
	}
	
	public static void stop(){
		for (StopRunnable runnable : threads) {
			runnable.stop();
			try {
				runnable.getThread().join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		threads.clear();
	}

}
