package causeandeffect.threadmanagement;

public abstract class StopRunnable implements Runnable {
	
	private boolean run=true;
	private Thread thread;
	
	public void setThread(Thread thread){
		this.thread=thread;
	}
	

	public Thread getThread() {
		return thread;
	}



	@Override
	public void run() {
		while(run){
			loop();
		}
	}

	protected abstract void loop();
	
	public void stop(){
		run=false;
	}

}
