package causeandeffect;

import causeandeffect.debug.DefaultDebug;
import causeandeffect.hulls.StrategyHull;
import causeandeffect.strategy.drivers.OrbitDriver;
import causeandeffect.strategy.gunners.StupidStraightGunner;
import causeandeffect.strategy.scanners.InfiniLockScanner;

public class ISuckAtMelee extends StrategyHull {
	
	private int RANGE = 200;

	public ISuckAtMelee() {
setDebug(new DefaultDebug());
		
		InfiniLockScanner scanner = new InfiniLockScanner();
		StupidStraightGunner gunner = new StupidStraightGunner();
		scanner.setHoldLockRange(RANGE);
		gunner.setMaxRangeToShoot(RANGE);
		
		
		addStrategy(new OrbitDriver());
		
		addStrategy(scanner);
		addStrategy(gunner);
	}
}
