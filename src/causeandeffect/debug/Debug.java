package causeandeffect.debug;

import java.awt.Graphics2D;

import causeandeffect.strategy.Strategy;

public interface Debug extends Strategy{
	
	public void addDebugPaint(DebugCommand debugPaintCommand);
	
	public void onPaint(Graphics2D g);

}
