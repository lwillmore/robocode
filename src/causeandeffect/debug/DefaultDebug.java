package causeandeffect.debug;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import robocode.ScannedRobotEvent;
import causeandeffect.strategy.BasicStrategy;

public class DefaultDebug extends BasicStrategy implements Debug {

	List<DebugCommand> otherDebugs = new ArrayList<DebugCommand>();
	public static int SCAN_CIRCLE = 40;
	public static int SCAN_WIDTH = SCAN_CIRCLE / 2;
	public static int LINE_LENGTH=400;
	int myX;
	int myY;

	Set<Point> scans = new HashSet<Point>();

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		// Calculate the angle to the scanned robot
		double angle = Math
				.toRadians((robot.getHeading() + e.getBearing()) % 360);
		// Calculate the coordinates of the robot
		int scannedX = (int) (robot.getX() + Math.sin(angle) * e.getDistance());
		int scannedY = (int) (robot.getY() + Math.cos(angle) * e.getDistance());
		scans.add(new Point(scannedX, scannedY));
	}

	@Override
	public void onPaint(Graphics2D g) {
		myX = (int) robot.getX();
		myY = (int) robot.getY();
		paintScans(g);
		paintRadar(g);
		paintTurret(g);
		paintOthers(g);
	}

	private void paintOthers(Graphics2D g) {
		for (DebugCommand debug : otherDebugs) {
			debug.execute(g);
		}
	}

	private void paintTurret(Graphics2D g) {
		g.setColor(new Color(0,255,0,200));
		double angle = Math
				.toRadians(robot.getGunHeading());
		int rX = (int) (robot.getX() + Math.sin(angle) * LINE_LENGTH);
		int rY = (int) (robot.getY() + Math.cos(angle) * LINE_LENGTH);
		g.drawLine(myX, myY, rX, rY);
	}

	private void paintRadar(Graphics2D g) {
		g.setColor(new Color(255,0,0,200));
		double angle = Math
				.toRadians(robot.getRadarHeading());
		int rX = (int) (robot.getX() + Math.sin(angle) * LINE_LENGTH);
		int rY = (int) (robot.getY() + Math.cos(angle) * LINE_LENGTH);
		g.drawLine(myX, myY, rX, rY);
	}

	private void paintScans(Graphics2D g) {
		g.setColor(new Color(0,255,255,200));
		for (Point scan : scans) {
			g.fillOval(scan.x - SCAN_WIDTH, scan.y - SCAN_WIDTH, SCAN_CIRCLE,
					SCAN_CIRCLE);
		}
		scans.clear();
	}

	@Override
	public void addDebugPaint(DebugCommand debugPaintCommand) {
		otherDebugs.add(debugPaintCommand);
	}

}
