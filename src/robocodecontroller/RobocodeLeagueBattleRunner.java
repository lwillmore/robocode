package robocodecontroller;

import java.util.Arrays;
import java.util.List;

import robocode.control.BattleSpecification;
import robocode.control.BattlefieldSpecification;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

public class RobocodeLeagueBattleRunner {
	
	private int roundsPerMatch = 5;
	private List<String> combatants = Arrays.asList(new String[]{"sample.RamFire","sample.Corners","sample.Crazy"});
	private int width= 800;
	private int height = 800;
	private RobocodeLeagueBattleObserver observer;
	
	public RobocodeLeagueBattleRunner(){
		this.observer=new RobocodeLeagueBattleObserver(combatants);
	}
	
	
	public RobocodeLeagueBattleRunner(List<String> combatants){
		this.combatants=combatants;
		this.observer=new RobocodeLeagueBattleObserver(combatants);
	}
	
	
	public RobocodeLeagueBattleRunner(int roundsPerMatch, List<String> combatants,
			int width, int height) {
		super();
		this.roundsPerMatch = roundsPerMatch;
		this.combatants = combatants;
		this.width = width;
		this.height = height;
		this.observer=new RobocodeLeagueBattleObserver(combatants);
	}
	
	public void run(){
		// Disable log messages from Robocode
        RobocodeEngine.setLogMessagesEnabled(true);

        // Create the RobocodeEngine
        RobocodeEngine engine = new RobocodeEngine(new java.io.File("C:/Robocode"));

        // Add our own battle listener to the RobocodeEngine 
        engine.addBattleListener(observer);

        // Show the Robocode battle view
        //engine.setVisible(true);

        // Setup the battle specification
        BattlefieldSpecification battlefield = new BattlefieldSpecification(width, height);
        
        runAllMatches(engine, battlefield);
        
        System.out.println("------------------------FINAL SCORES-----------------------");
        observer.printScores();
        engine.close();
	}


	private void runAllMatches(RobocodeEngine engine,
			BattlefieldSpecification battlefield) {
		for (int combatantA = 0; combatantA < combatants.size(); combatantA++) {
        	String combatantAname = combatants.get(combatantA);
        	playEveryoneElse(engine, battlefield, combatantA, combatantAname);			
		}
	}


	private void playEveryoneElse(RobocodeEngine engine,
			BattlefieldSpecification battlefield, int combatantA,
			String combatantAname) {
		for (int combatantB = combatantA+1; combatantB < combatants.size(); combatantB++) {
			String combatantBname = combatants.get(combatantB);
			RobotSpecification[] selectedRobots = getSpecifications(combatantAname, combatantBname, engine);
					engine.getLocalRepository(combatantAname+","+combatantBname);
			for (RobotSpecification robotSpecification : selectedRobots) {
				System.out.println(robotSpecification.getName());
			}
			BattleSpecification battleSpec = new BattleSpecification(roundsPerMatch, battlefield, selectedRobots);
			playEachRound(engine, battleSpec);					
		}
	}


	private RobotSpecification[] getSpecifications(String combatantAname,
			String combatantBname, RobocodeEngine engine) {
		RobotSpecification[] specifications = engine.getLocalRepository();
		RobotSpecification[] combatants = new RobotSpecification[2];
		boolean foundA = false;
		boolean foundB = false;
		for (int i = 0; i < specifications.length; i++) {
			RobotSpecification robotSpecification = specifications[i];
			if(robotSpecification.getName().equals(combatantAname)){
				combatants[0]=robotSpecification;
				foundA=true;
			}
			if(robotSpecification.getName().equals(combatantBname)){
				combatants[1]=robotSpecification;
				foundB=true;
			}
		}
		if(!foundA){
			System.out.println("Could not find "+combatantAname);
		}
		if(!foundB){
			System.out.println("Could not find "+combatantBname);
		}
		return combatants;
	}


	private void playEachRound(RobocodeEngine engine,
			BattleSpecification battleSpec) {
		for (int round = 0; round < roundsPerMatch; round++) {
			engine.runBattle(battleSpec, true);	
		}
	}

	public void setRoundsPerMatch(int roundsPerMatch) {
		this.roundsPerMatch = roundsPerMatch;
	}

	public void setCombatants(List<String> combatants) {
		this.combatants = combatants;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public static void main(String[] args) {
		List<String> combatants = Arrays.asList("Cyberdyne.S150,Cyberdyne.S151".split(","));
		RobocodeLeagueBattleRunner runner = new RobocodeLeagueBattleRunner(combatants);
		runner.run();
	}

}
