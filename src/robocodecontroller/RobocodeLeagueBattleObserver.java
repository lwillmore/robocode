package robocodecontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import robocode.BattleResults;
import robocode.control.events.BattleCompletedEvent;

public class RobocodeLeagueBattleObserver extends AbstractBattleObserver {
	
	private List<String> allCombatants = new ArrayList<String>();
	
	private Map<String,Map<String,Integer>> scoreSheet = new HashMap<String,Map<String,Integer>>();
	
	private Set<String> matchCombatants = new HashSet<String>();
	
	private int longestName=0;
	

	

	public RobocodeLeagueBattleObserver(List<String> allCombatants) {
		super();
		for (String combatant : allCombatants) {
			String comNoAstrix = combatant.replaceAll("\\*", "");
			longestName = ((comNoAstrix.length()>longestName)? comNoAstrix.length() : longestName);
			this.allCombatants.add(comNoAstrix);
			System.out.println(comNoAstrix);
		}
		for (String combatantA : this.allCombatants) {
			Map<String,Integer> scoreLine = new HashMap<String, Integer>();
			for (String CombatantB : this.allCombatants) {
				scoreLine.put(CombatantB, 0);
			}
			scoreSheet.put(combatantA, scoreLine);
		}
	}



	@Override
	public void onBattleCompleted(BattleCompletedEvent event) {
		BattleResults[] results = event.getSortedResults();
		
		String winner = results[0].getTeamLeaderName();
		String loser = results[1].getTeamLeaderName();
		
		System.out.println("WINNER: "+winner+"\tLOSER: "+loser);
		
		updateScores(winner,loser);
		
		if(newFighters(winner,loser)){
			printScores();
		}
		
	}

	public void printScores() {
		System.out.println("--------------------------------------------------------------------------------");
		printHeadings();
		for (String combatantA : allCombatants) {
			StringBuilder aScores = new StringBuilder();
			aScores.append(combatantA);
			appendTabs(aScores,(longestName - combatantA.length())/6 +1);
			int total =0;
			Map<String, Integer> scoreline = scoreSheet.get(combatantA);
			for (String CombatantB : allCombatants) {
				Integer score = scoreline.get(CombatantB);
				aScores.append(score);
				appendTabs(aScores,CombatantB.length()/6);
				total+=score;
			}
			aScores.append(""+total);
			System.out.println(aScores);
		}
		System.out.println("--------------------------------------------------------------------------------");
	}



	private void printHeadings() {
		StringBuilder headings = new StringBuilder();
		appendTabs(headings,longestName/6);
		for (String combatantB : allCombatants) {
			headings.append(combatantB+"\t");
		}
		headings.append("TOTAL");
		System.out.println(headings.toString());
	}



	private void appendTabs(StringBuilder string, int tabCount) {
		for(int i = 0; i<tabCount;i++){
			string.append("\t");
		}	
	}



	private void updateScores(String winner, String loser) {
		for (String winnerName : scoreSheet.keySet()) {
			if(winner.contains(winnerName)){
				Map<String, Integer> scoreline = scoreSheet.get(winnerName);
				for (String loserName : scoreline.keySet()) {
					if(loser.contains(loserName)){
						scoreline.put(loserName, scoreline.get(loserName)+1);
					}
				}
			}
		}
		//scoreSheet.get(winner).put(loser, scoreSheet.get(winner).get(loser)+1);
	}



	private boolean newFighters(String winner, String loser) {		
		boolean result = (!matchCombatants.contains(loser) || !matchCombatants.contains(winner));
		matchCombatants.clear();
		matchCombatants.add(loser);
		matchCombatants.add(winner);
		return result;
	}
	
	

}
