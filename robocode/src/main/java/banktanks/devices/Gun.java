package banktanks.devices;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import banktanks.controllers.BasicController;
import banktanks.controllers.aiming.AimData;
import banktanks.controllers.aiming.Aiming;
import banktanks.debug.DebugCommand;
import banktanks.util.Log;
import banktanks.util.Log.LogLevel;
import banktanks.util.MathUtil;
import banktanks.util.Vector2;

public class Gun extends BasicController {

	private final double TARGET_RADIUS = 10;
	boolean firing = false;
	double power;
	Vector2 target;
	private Vector2 pointA;
	private Vector2 pointB;

	@Override
	public void init() {
		robot.debug.addDebugPaint(new GunPaint(this));
	}

	public void fireAt(ScannedRobotData scanData) {
		Log.log(this.getClass(), LogLevel.DEBUG, "Firing?: " + firing);
		if (!firing) {			
			AimData aim = robot.getStrategy(Aiming.class).getAimData(scanData);			
			if (aim.power >= 0.1) {
				Log.log(this.getClass(), LogLevel.INFO, "Firing at: "
						+ scanData.getEvent().getName());
				Log.log(this.getClass(), LogLevel.INFO, aim.toString());
				firing = true;
				power = aim.power;
				target = aim.position;
			}
		}
	}

	@Override
	public void update() {
		if (firing) {
			if (onTarget() && robot.getGunHeat() <= 0) {
				fire();
			} else {
				lineUpOnTarget();
			}
		}
	}

	private void lineUpOnTarget() {
		Log.log(this.getClass(), LogLevel.DEBUG, "Lining up Gun...");
		double degreeHeading = robot.getGunHeading();
		double requiredHeading = MathUtil.degreesFromVector(target.copy()
				.subtract(robot.getPosition()));
		Log.log(this.getClass(), LogLevel.DEBUG, "Current Gun Heading: "
				+ degreeHeading + " Required " + requiredHeading);
		if (degreeHeading > requiredHeading) {
			double left = degreeHeading - requiredHeading;
			double right = 360 - degreeHeading + requiredHeading;
			if (right > left) {
				robot.setTurnGunLeft(left);
			} else {
				robot.setTurnGunRight(right);
			}
		} else {
			double left = degreeHeading + 360 - requiredHeading;
			double right = requiredHeading - degreeHeading;
			if (right > left) {
				robot.setTurnGunLeft(left);
			} else {
				robot.setTurnGunRight(right);
			}
		}
	}

	private void fire() {
		robot.setFire(power);
		firing = false;
	}

	private boolean onTarget() {
		pointA = robot.getPosition();
		Vector2 headingVector = MathUtil.getVectorFromRadians(robot
				.getGunHeadingRadians());
		Log.log(this.getClass(), LogLevel.DEBUG, "Current Gun Heading: "
				+ headingVector);
		headingVector.mult(new Vector2(robot.getBattleFieldWidth(), robot
				.getBattleFieldHeight()).length());
		pointB = pointA.copy().add(headingVector);
		boolean rayIntercectsSphere = MathUtil.doesRayIntercectsSphere(pointA,
				pointB, target.copy(), TARGET_RADIUS);
		if (rayIntercectsSphere) {
			Log.log(this.getClass(), LogLevel.DEBUG, "Hit : "
					+ rayIntercectsSphere + " on "+target);
		}
		return rayIntercectsSphere;
	}

	public class GunPaint implements DebugCommand {

		Gun myGun;

		public GunPaint(Gun myGun) {
			super();
			this.myGun = myGun;
		}

		@Override
		public void execute(Graphics2D g) {
			if (myGun.firing) {
				g.setColor(Color.green);
				g.fill(new Ellipse2D.Double(myGun.target.x, myGun.target.y,
						myGun.TARGET_RADIUS, myGun.TARGET_RADIUS));
				g.setColor(Color.PINK);
				if (pointA != null) {
					g.drawLine((int) pointA.x, (int) pointA.y, (int) pointB.x,
							(int) pointB.y);
				}
			}
		}

	}

	public int turnsToAlignOn(Vector2 pointBEnemyRay) {
		double degreeHeading = robot.getGunHeading();
		double requiredHeading = MathUtil.degreesFromVector(target.copy()
				.subtract(robot.getPosition()));
		Log.log(this.getClass(), LogLevel.DEBUG, "Current Gun Heading: "
				+ degreeHeading + " Required " + requiredHeading);
		if (degreeHeading > requiredHeading) {
			double left = degreeHeading - requiredHeading;
			double right = 360 - degreeHeading + requiredHeading;
			if (right > left) {
				return (int) (left/20);
			} else {
				return (int) (right/20);
			}
		} else {
			double left = degreeHeading + 360 - requiredHeading;
			double right = requiredHeading - degreeHeading;
			if (right > left) {
				return (int) (left/20);
			} else {
				return (int) (right/20);
			}
		}
	}

}
