package banktanks.devices;

import robocode.ScannedRobotEvent;
import banktanks.hulls.StrategyHull;
import banktanks.util.Vector2;

public class ScannedRobotData {
	
	private StrategyHull robot;
	private String name;
	private long createdOn;
	private Vector2 positionWhenScanned;
	private double headingWhenScanned;
	private ScannedRobotEvent event;
	
	public ScannedRobotData(StrategyHull source, ScannedRobotEvent event) {
		robot=source;
		createdOn = source.getTime();
		this.name = event.getName();
		this.event = event;
		positionWhenScanned = source.getPosition();
		headingWhenScanned = source.getHeading();
	}
	
	

	public double getHeadingWhenScanned() {
		return headingWhenScanned;
	}



	public boolean isOlderThan(int gameTurns) {
		return createdOn+gameTurns < robot.getTime();
	}
	
	public long getAge(){
		return robot.getTime()-createdOn;
	}

	public String getName() {
		return name;
	}

	public ScannedRobotEvent getEvent() {
		return event;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public Vector2 getMyPositionWhenScanned() {
		return positionWhenScanned;
	}
	
	public Vector2 getPositionOfScannedRobot(){
		double angle = Math
				.toRadians((headingWhenScanned + event.getBearing()) % 360);
		int scannedX = (int) (positionWhenScanned.x + Math.sin(angle) * event.getDistance());
		int scannedY = (int) (positionWhenScanned.y + Math.cos(angle) * event.getDistance());
		Vector2 point = new Vector2(scannedX,scannedY);
		return point;
	}
	
	
	
	

}
