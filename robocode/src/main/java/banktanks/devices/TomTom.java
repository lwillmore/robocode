package banktanks.devices;

import java.util.List;
import java.util.Stack;

import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import banktanks.controllers.BasicController;
import banktanks.util.Log;
import banktanks.util.Log.LogLevel;
import banktanks.util.MathUtil;
import banktanks.util.Vector2;

public class TomTom extends BasicController {

	private static final int BACKUP_DISTANCE = 30;
	double arriveThreshold = 5;
	boolean backup = false;
	boolean backingUp = false;

	Stack<Vector2> route = new Stack<Vector2>();
	private Vector2 currentDestination;

	public void driveTo(Vector2 destination) {
		route.clear();
		route.push(destination);
	}

	public void driveRoute(List<Vector2> route) {
		route.clear();
		for (int i = route.size(); i > 0; i--) {
			this.route.push(route.get(i));
		}
	}

	@Override
	public void update() {
		if (backup) {
			backUp();
		} else if (isThereSomeWhereToGo()) {
			getThere();
		}
	}

	private void backUp() {
		if (!backingUp) {
			robot.setBack(BACKUP_DISTANCE);
			backingUp = true;
		} else if (robot.getDistanceRemaining() == 0) {
			backup = false;
			backingUp = false;
		}
	}

	private void getThere() {
		if (aimingAtCurrentDestination()) {
			driveForward();
		} else {
			lineUp();
		}
	}

	private void driveForward() {
		if (robot.getDistanceRemaining() <= 0) {
			Log.log(this.getClass(), LogLevel.DEBUG, "Driving forward...");
			Vector2 myPosition = new Vector2(robot.getX(), robot.getY());
			robot.setAhead(currentDestination.copy().subtract(myPosition)
					.length());
		}
	}

	private void lineUp() {
		if (robot.getTurnRemaining() == 0) {
			Log.log(this.getClass(), LogLevel.DEBUG, "Lining up...");
			double degreeHeading = robot.getHeading();
			double requiredHeading = MathUtil
					.degreesFromVector(currentDestination.copy().subtract(
							robot.getPosition()));
			Log.log(this.getClass(), LogLevel.DEBUG, "Current Heading: "+degreeHeading + " Required "+requiredHeading);
			if (degreeHeading > requiredHeading) {
				double left = degreeHeading - requiredHeading;
				double right = 360 - degreeHeading + requiredHeading;
				if (right > left) {
					robot.setTurnLeft(left);
				} else {
					robot.setTurnRight(right);
				}
			} else {
				double left = degreeHeading + 360 -  requiredHeading;
				double right = requiredHeading - degreeHeading;
				if (right > left) {
					robot.setTurnLeft(left);
				} else {
					robot.setTurnRight(right);
				}
			}
		}
	}

	private boolean aimingAtCurrentDestination() {
		double degreeHeading = robot.getHeading();
		Vector2 myPosition = new Vector2(robot.getX(), robot.getY());
		double requiredHeading = MathUtil.degreesFromVector(currentDestination
				.copy().subtract(myPosition));
		Log.log(this.getClass(), LogLevel.DEBUG, "Current heading:"
				+ degreeHeading + " required: " + requiredHeading);
		return MathUtil.round(degreeHeading,2) == MathUtil.round(requiredHeading,2);
	}

	public boolean isThereSomeWhereToGo() {
		if(currentDestination != null && haveArrivedAtCurrentDestination()){
			Log.log(this.getClass(), LogLevel.DEBUG, "Arrived at Destination");
			currentDestination = null;
		}
		while (currentDestination == null
				&& route.size() > 0) {
			Log.log(this.getClass(), LogLevel.DEBUG, "Popping next destination");
			currentDestination = route.pop();
			if(haveArrivedAtCurrentDestination()){
				Log.log(this.getClass(), LogLevel.DEBUG, "Arrived at Destination");
				currentDestination = null;
			}
		}
		return currentDestination != null;
	}

	private boolean haveArrivedAtCurrentDestination() {
		double difX = Math.abs(robot.getX() - currentDestination.x);
		double difY = Math.abs(robot.getY() - currentDestination.y);
		boolean arrived = difX < arriveThreshold && difY < arriveThreshold;
		return arrived;
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		backup=true;
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		backup=true;
	}
	
	

}
