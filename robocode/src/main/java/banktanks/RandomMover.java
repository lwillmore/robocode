package banktanks;

import banktanks.controllers.movement.RandomMovement;
import banktanks.debug.DefaultDebug;
import banktanks.devices.TomTom;
import banktanks.hulls.StrategyHull;

public class RandomMover extends StrategyHull {


	public RandomMover() {

		addStrategy(new TomTom());
		
		addStrategy(new RandomMovement());
		
	}
}