package banktanks.debug;

import java.awt.Graphics2D;

import banktanks.controllers.Controller;

public interface Debug extends Controller{
	
	public void addDebugPaint(DebugCommand debugPaintCommand);
	
	public void onPaint(Graphics2D g);

}
