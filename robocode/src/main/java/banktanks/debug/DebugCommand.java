package banktanks.debug;

import java.awt.Graphics2D;

public interface DebugCommand {
	
	public void execute(Graphics2D g);

}
