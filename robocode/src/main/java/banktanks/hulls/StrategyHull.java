package banktanks.hulls;

import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;

import robocode.DeathEvent;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import banktanks.controllers.Controller;
import banktanks.debug.DefaultDebug;
import banktanks.debug.NullDebug;
import banktanks.devices.ScannedRobotData;
import banktanks.util.Vector2;

public abstract class StrategyHull extends BasicHull {
	
	Map<Class<? extends Controller>,Controller> strategies = new HashMap<Class<? extends Controller>, Controller>();
	public DefaultDebug debug = new NullDebug(false);
	Vector2 myPosition = new Vector2();
	public ScannedRobotData target;

	public void setDebug(DefaultDebug debugStrategy){
		debug=debugStrategy;
		debug.setTarget(this);
		addStrategy(debug);
	}

	@Override
	public void loop() {
		myPosition.set(this.getX(),this.getY());
		for (Controller strategy : strategies.values()) {
			strategy.update();
		}
		execute();
	}

	@Override
	public void initialise() {
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		for (Controller strategy : strategies.values()) {
			strategy.init();
		}
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		for (Controller strategy : strategies.values()) {
			strategy.onScannedRobot(event);
		}
	}

	@Override
	public void onHitByBullet(HitByBulletEvent event) {
		for (Controller strategy : strategies.values()) {
			strategy.onHitByBullet(event);
		}
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		for (Controller strategy : strategies.values()) {
			strategy.onHitRobot(event);
		}
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		for (Controller strategy : strategies.values()) {
			strategy.onHitWall(event);
		}
	}

	@Override
	public void onPaint(Graphics2D g) {
		debug.onPaint(g);
	}
	
	
	
	
	
	@Override
	public void onRobotDeath(RobotDeathEvent event) {
		for (Controller strategy : strategies.values()) {
			strategy.onRobotDeath(event);
		}
		super.onRobotDeath(event);
	}

	@Override
	public void onRoundEnded(RoundEndedEvent event) {
		for (Controller strategy : strategies.values()) {
			strategy.onRoundEnded(event);
		}
		super.onRoundEnded(event);
	}

	public void addStrategy(Controller strategy, Class<? extends Controller> type){
		strategies.put(type, strategy);
		strategy.setTarget(this);
		for (Controller controller : strategy.getDependentStrategies()) {
			addStrategy(controller);
		}
	}
	
	public void addStrategy(Controller strategy){
		addStrategy(strategy, strategy.getClass());
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Controller> T getStrategy(Class<T> clazz){
		return (T) strategies.get(clazz);
	}
	
	public void removeStrategy(Class<?> clazz){
		strategies.remove(clazz.getName());
	}

	public Vector2 getPosition() {
		return myPosition.copy();
	}

	@Override
	public void onDeath(DeathEvent event) {
		super.onDeath(event);
		target = null;
	}

	@Override
	public void onWin(WinEvent event) {
		super.onWin(event);
		target = null;
	}
	
	

}
