package banktanks.hulls;

import robocode.AdvancedRobot;

public abstract class BasicHull extends AdvancedRobot {

	@Override
	public void run() {
		initialise();
		while(true){
			loop();
		}
	}

	public abstract void loop();

	public abstract void initialise();
	
	
	
	
	

}
