package banktanks.util;

import java.awt.Point;

public class Vector2 {

	public double x = 0;
	public double y = 0;

	public Vector2(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Vector2 nor() {
		double length = Math.sqrt(this.x * this.x + this.y * this.y);
		if (length != 0) {
			x = x / length;
			y = y / length;
		}
		return this;
	}

	public Vector2() {
	}

	public Vector2 subtract(Vector2 vector) {
		this.x -= vector.x;
		this.y -= vector.y;
		return this;
	}

	public double length() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}

	public Vector2 copy() {
		return new Vector2(x, y);
	}

	@Override
	public String toString() {
		return "[" + x + ", " + y + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2 other = (Vector2) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	public Vector2 mult(double scalar) {
		x *= scalar;
		y *= scalar;
		return this;
	}

	public Vector2 add(Vector2 vector) {
		x += vector.x;
		y += vector.y;
		return this;
	}

	public void set(double x2, double y2) {
		x = x2;
		y = y2;
	}

}
