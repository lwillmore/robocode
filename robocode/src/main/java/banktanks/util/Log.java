package banktanks.util;

public class Log {

	public enum LogLevel {
		INFO, DEBUG, TRACE
	}

	public static LogLevel level = LogLevel.INFO;

	public static void log(Class<?> clazz, LogLevel level, String message) {
		if (level.ordinal() <= Log.level.ordinal()) {
			System.out.println(clazz.getName() + ": " + message);
		}
	}

}
