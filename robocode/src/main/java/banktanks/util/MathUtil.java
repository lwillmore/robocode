package banktanks.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import banktanks.util.Log.LogLevel;

public class MathUtil {
	
	public static double degreesToRadians = Math.PI/180;
	public static double radiansToDegrees = 180/Math.PI;
	static Random rand = new Random(System.currentTimeMillis());
	
	public static double degreesFromVector(Vector2 direction) {
		double degrees = radiansFromVector(direction)*radiansToDegrees;
		if(degrees<0){
			return 360+degrees;
		}
		Log.log(MathUtil.class,LogLevel.TRACE, "Degrees: "+degrees+" from "+direction);
		return degrees;
	}
	
	public static double radiansFromVector(Vector2 direction){	
		double radians = Math.atan2(direction.x, direction.y);
		Log.log(MathUtil.class,LogLevel.TRACE, "Radians: "+radians+" from "+direction);
		return radians;
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
    
	public static int randInt(int min, int max) {
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public static double randDouble(double min, double max){
		return min+(rand.nextDouble()*(max-min));
	}
	
	public static boolean doesRayIntercectsSphere(Vector2 pointA,
            Vector2 pointB, Vector2 center, double radius) {
        double baX = pointB.x - pointA.x;
        double baY = pointB.y - pointA.y;
        double caX = center.x - pointA.x;
        double caY = center.y - pointA.y;

        double a = baX * baX + baY * baY;
        double bBy2 = baX * caX + baY * caY;
        double c = caX * caX + caY * caY - radius * radius;

        double pBy2 = bBy2 / a;
        double q = c / a;

        double disc = pBy2 * pBy2 - q;
        if (disc < 0) {
            return false;
        }
        return true;
    }
	
	
	public static List<Vector2> whereDoesRayIntercectsSphere(Vector2 pointA,
            Vector2 pointB, Vector2 center, double radius) {
        double baX = pointB.x - pointA.x;
        double baY = pointB.y - pointA.y;
        double caX = center.x - pointA.x;
        double caY = center.y - pointA.y;

        double a = baX * baX + baY * baY;
        double bBy2 = baX * caX + baY * caY;
        double c = caX * caX + caY * caY - radius * radius;

        double pBy2 = bBy2 / a;
        double q = c / a;

        double disc = pBy2 * pBy2 - q;
        if (disc < 0) {
            return Collections.emptyList();
        }
        // if disc == 0 ... dealt with later
        double tmpSqrt = Math.sqrt(disc);
        double abScalingFactor1 = -pBy2 + tmpSqrt;
        double abScalingFactor2 = -pBy2 - tmpSqrt;

        Vector2 p1 = new Vector2(pointA.x - baX * abScalingFactor1, pointA.y
                - baY * abScalingFactor1);
        if (disc == 0) { // abScalingFactor1 == abScalingFactor2
            return Collections.singletonList(p1);
        }
        Vector2 p2 = new Vector2(pointA.x - baX * abScalingFactor2, pointA.y
                - baY * abScalingFactor2);
        return Arrays.asList(p1, p2);
    }
	

	public static Vector2 getVectorFromRadians(double gunHeadingRadians) {
		double shiftedRadians = gunHeadingRadians-2*Math.PI;
		return new Vector2(MathUtil.round(Math.sin(shiftedRadians),2),MathUtil.round(Math.cos(shiftedRadians),2));
	}

}
