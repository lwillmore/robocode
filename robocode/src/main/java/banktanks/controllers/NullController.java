package banktanks.controllers;

import java.util.Collections;
import java.util.List;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import banktanks.hulls.StrategyHull;

public class NullController implements Controller {

	@Override
	public void update() {
		
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		
	}

	@Override
	public void onHitByBullet(HitByBulletEvent event) {
		
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		
	}

	@Override
	public void setTarget(StrategyHull strategyHull) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRobotDeath(RobotDeathEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRoundEnded(RoundEndedEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		
		return "Null";
	}

	@Override
	public List<Controller> getDependentStrategies() {
		return Collections.emptyList();
	}
	
	
	
	

}
