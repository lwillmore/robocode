package banktanks.controllers.scanners;

import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import banktanks.controllers.BasicController;
import banktanks.devices.Gun;
import banktanks.devices.ScannedRobotData;

public class TargetSelectionScanner extends BasicController {

	private long lastAdjustment = 0;

	@Override
	public void update() {
		if (robot.target == null) {
			robot.setTurnRadarRight(Double.POSITIVE_INFINITY);
		} else if (notLocked()) {
			robot.setTurnRadarRight(Double.POSITIVE_INFINITY);
		}
	}

	private boolean notLocked() {
		long currentTime = robot.getTime();
		if ((currentTime - lastAdjustment) > 10) {
			return true;
		}
		return false;
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		if (robot.target != null
				&& e.getName().equals(robot.target.getEvent().getName())) {
			double radarTurn = // Absolute bearing to target
			robot.getHeadingRadians() + e.getBearingRadians()
			// Subtract current radar heading to get turn required
					- robot.getRadarHeadingRadians();
			robot.setTurnRadarRightRadians(Utils.normalRelativeAngle(radarTurn));
			fire(new ScannedRobotData(robot, e));
			lastAdjustment = robot.getTime();
		}
	}

	protected void fire(ScannedRobotData data) {
		Gun gun = robot.getStrategy(Gun.class);
		gun.fireAt(data);
	}

}
