package banktanks.controllers.scanners;

import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import banktanks.controllers.BasicController;

public class InfiniLockScanner extends BasicController {

	private int holdLockRange = 200;
	private long lastAdjustment = 0;

	@Override
	public void init() {
		robot.turnRadarRightRadians(Double.POSITIVE_INFINITY);
	}

	@Override
	public void update() {
		if (notScanning()) {
			robot.setTurnRadarRightRadians(Double.POSITIVE_INFINITY);
		}
		super.update();
	}

	private boolean notScanning() {
		long currentTime = robot.getTime();
		if ((currentTime - lastAdjustment) > 2) {
			return true;
		}
		return false;
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		if (inRange(e)) {
			double radarTurn =
			// Absolute bearing to target
			robot.getHeadingRadians() + e.getBearingRadians()
			// Subtract current radar heading to get turn required
					- robot.getRadarHeadingRadians();

			robot.setTurnRadarRightRadians(Utils.normalRelativeAngle(radarTurn));
			lastAdjustment = robot.getTime();
		} else {
			robot.setTurnRadarRightRadians(Double.POSITIVE_INFINITY);
			lastAdjustment = robot.getTime();
		}
	}

	private boolean inRange(ScannedRobotEvent e) {
		System.out.println("Enemey scanned range = " + e.getDistance());
		return (e.getDistance() < holdLockRange);
	}

	public void setHoldLockRange(int holdLockRange) {
		this.holdLockRange = holdLockRange;
	}

}
