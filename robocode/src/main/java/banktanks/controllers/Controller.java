package banktanks.controllers;

import java.util.List;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import banktanks.hulls.StrategyHull;

public interface Controller {

	public void update();

	public void init();

	public void onScannedRobot(ScannedRobotEvent e);

	public void onHitByBullet(HitByBulletEvent event);

	public void onHitRobot(HitRobotEvent event);

	public void onHitWall(HitWallEvent event);

	void setTarget(StrategyHull strategyHull);

	public void onRobotDeath(RobotDeathEvent event);

	public void onRoundEnded(RoundEndedEvent event);
	
	public String getName();

	public List<? extends Controller> getDependentStrategies();

}
