package banktanks.controllers.movement;

import banktanks.controllers.BasicController;
import banktanks.devices.TomTom;
import banktanks.util.MathUtil;
import banktanks.util.Vector2;

public class RandomMovement extends BasicController {
	double cornerInset = 20;
	

	@Override
	public void update() {
		TomTom tt = robot.getStrategy(TomTom.class);
		if(!tt.isThereSomeWhereToGo()){
			tt.driveTo(someWhereRandom());
		}
	}

	private Vector2 someWhereRandom() {
		double x = MathUtil.randDouble(0+cornerInset, robot.getBattleFieldWidth()-cornerInset);
		double y = MathUtil.randDouble(0+cornerInset, robot.getBattleFieldHeight()-cornerInset);
		return new Vector2(x,y);
	}
	
	

}