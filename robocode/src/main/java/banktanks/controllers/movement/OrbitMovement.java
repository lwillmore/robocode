package banktanks.controllers.movement;

import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import banktanks.controllers.BasicController;

public class OrbitMovement extends BasicController {

	int boundaryWidth = 10;
	double left = 0;
	double right;
	double bottom = 0;
	double top;
	int forwardBack = 1;
	boolean gettingOutOfBorder = false;

	@Override
	public void update() {
		checkBoundaries();
		robot.setAhead(60 * forwardBack);
	}

	@Override
	public void init() {
		super.init();
		boundaryWidth = Math.max(boundaryWidth, robot.getSentryBorderSize());
		right = robot.getBattleFieldWidth() - boundaryWidth;
		top = robot.getBattleFieldHeight() - boundaryWidth;
		left = 0 + boundaryWidth;
		bottom = 0 + boundaryWidth;
	}

	private void checkBoundaries() {
		
		if (!gettingOutOfBorder) {
			double myx = robot.getX();
			double myy = robot.getY();
			if (myx > right || myx < left || myy > top || myy < bottom) {
				forwardBack = forwardBack * -1;
				gettingOutOfBorder = true;
			}
		}
		else if(gettingOutOfBorder){
			double myx = robot.getX();
			double myy = robot.getY();
			if (myx < right && myx > left && myy < top && myy > bottom) {
				gettingOutOfBorder = false;
			}
		}
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		robot.setTurnRight(e.getBearing() + 90);
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		forwardBack *= -1;
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		forwardBack *= -1;
	}

}
