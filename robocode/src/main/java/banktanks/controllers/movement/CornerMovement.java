package banktanks.controllers.movement;

import java.util.ArrayList;
import java.util.List;

import banktanks.controllers.BasicController;
import banktanks.devices.TomTom;
import banktanks.util.Log;
import banktanks.util.Log.LogLevel;
import banktanks.util.Vector2;

public class CornerMovement extends BasicController {
	double cornerInset = 20;
	List<Vector2> corners = new ArrayList<Vector2>();
	int currentCorner = 0;

	@Override
	public void init() {
		double height = robot.getBattleFieldHeight();
		double width = robot.getBattleFieldWidth();
		corners.add(new Vector2(0 + cornerInset, 0 + cornerInset));
		corners.add(new Vector2(0 + cornerInset, height - cornerInset));
		corners.add(new Vector2(width - cornerInset, height - cornerInset));
		corners.add(new Vector2(width - cornerInset, 0 + cornerInset));
	}

	@Override
	public void update() {
		TomTom tt = robot.getStrategy(TomTom.class);
		if (!tt.isThereSomeWhereToGo()) {
			Log.log(this.getClass(),LogLevel.INFO, "No Where to go, setting next corner "+corners.get(currentCorner));
			tt.driveTo(corners.get(currentCorner));
			currentCorner = (currentCorner + 1) % 4;
		}
	}

}
