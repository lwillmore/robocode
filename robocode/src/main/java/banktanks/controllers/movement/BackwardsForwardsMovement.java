package banktanks.controllers.movement;

import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import banktanks.controllers.BasicController;

public class BackwardsForwardsMovement extends BasicController {
	
	int forwardBack = 1;

	@Override
	public void update() {
		robot.setAhead(60 * forwardBack);
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		forwardBack*=-1;
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		forwardBack*=-1;
	}
	

}
