package banktanks.controllers.targeting;

import banktanks.controllers.BasicController;
import banktanks.devices.Gun;
import banktanks.devices.ScannedRobotData;

public class BasicTargeting extends BasicController{
	
	protected void fire(ScannedRobotData data) {
		Gun gun = robot.getStrategy(Gun.class);
		gun.fireAt(data);
	}

}
