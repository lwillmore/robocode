package banktanks.controllers.targeting;

import banktanks.devices.ScannedRobotData;

public class TargetStrongest extends SelectiveTargeting {

	@Override
	protected ScannedRobotData select(ScannedRobotData target,
			ScannedRobotData latest) {
		if(latest.getEvent().getEnergy()>target.getEvent().getEnergy()){
			return latest;
		}
		return target;
	}

	

}
