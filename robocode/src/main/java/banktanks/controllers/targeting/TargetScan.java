package banktanks.controllers.targeting;

import java.util.Arrays;
import java.util.List;

import robocode.ScannedRobotEvent;
import banktanks.controllers.Controller;
import banktanks.controllers.scanners.InfiniLockScanner;
import banktanks.devices.ScannedRobotData;

public class TargetScan extends BasicTargeting{
	
	@Override
	public List<? extends Controller> getDependentStrategies() {
		return Arrays.asList(new InfiniLockScanner());
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(new ScannedRobotData(robot,e));
	}

	

	
	
	

}
