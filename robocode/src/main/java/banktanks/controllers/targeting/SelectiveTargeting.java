package banktanks.controllers.targeting;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import robocode.RobotDeathEvent;
import banktanks.controllers.Controller;
import banktanks.controllers.scanners.TargetSelectionScanner;
import banktanks.devices.ScannedRobotData;
import banktanks.devices.ScannedRobotMemory;
import banktanks.util.Log;
import banktanks.util.Log.LogLevel;

public abstract class SelectiveTargeting extends BasicTargeting {
	
	@Override
	public List<? extends Controller> getDependentStrategies() {
		return Arrays.asList(new TargetSelectionScanner());
	}



	@Override
	public void update() {
			robot.target = filterTargets();
	}
	
	

	@Override
	public void onRobotDeath(RobotDeathEvent event) {
		if(event.getName().equals(robot.target.getName())){
			robot.target=null;
		}
	}



	protected ScannedRobotData filterTargets() {
		Map<String, Queue<ScannedRobotData>> memory = robot.getStrategy(
				ScannedRobotMemory.class).getMemory();
		ScannedRobotData target = null;
		for (Queue<ScannedRobotData> enemyHistory : memory.values()) {
			ScannedRobotData latest = enemyHistory.peek();
			if (!latest.isOlderThan(60)) {
				if (target == null) {
					target = latest;
				} else {
					target = select(target, latest);
				}
			}
		}
		if(target!=null){
			Log.log(getClass(), LogLevel.INFO, "Returning "+target.getName());
		}
		return target;
	}

	protected abstract ScannedRobotData select(ScannedRobotData target,
			ScannedRobotData latest);

}
