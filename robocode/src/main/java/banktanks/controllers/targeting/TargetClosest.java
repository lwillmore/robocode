package banktanks.controllers.targeting;

import banktanks.devices.ScannedRobotData;

public class TargetClosest extends SelectiveTargeting {

	@Override
	protected ScannedRobotData select(ScannedRobotData target,
			ScannedRobotData latest) {
		if(latest.getEvent().getDistance()<target.getEvent().getDistance()){
			return latest;
		}
		return target;
	}

	

}
