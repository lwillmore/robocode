package banktanks.controllers.aiming;

import banktanks.devices.ScannedRobotData;

public class AimingStraight extends Aiming{
	
	double power = 0.5;

	@Override
	public AimData getAimData(ScannedRobotData scan) {
		return new AimData(scan.getPositionOfScannedRobot(),power);
	}

}
