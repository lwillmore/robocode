package banktanks.controllers.aiming;

import banktanks.controllers.BasicController;
import banktanks.devices.ScannedRobotData;

public abstract class Aiming extends BasicController{
	
	public abstract AimData getAimData(ScannedRobotData scan);

}
