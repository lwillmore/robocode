package banktanks.controllers.aiming;

import banktanks.util.Vector2;

public class AimData {
	
	public Vector2 position;
	
	public double power;

	public AimData(Vector2 position, double power) {
		super();
		this.position = position;
		this.power = power;
	}

	@Override
	public String toString() {
		return "AimData [position=" + position + ", power=" + power + "]";
	}
	
	
	
	
}
