package banktanks.controllers.aiming;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.util.List;

import banktanks.debug.DebugCommand;
import banktanks.devices.Gun;
import banktanks.devices.ScannedRobotData;
import banktanks.util.MathUtil;
import banktanks.util.Vector2;

public class AimingLead extends Aiming {

	private Vector2 pointAEnemyRay;
	private Vector2 pointBEnemyRay;

	Double radius;
	Vector2 centre;

	boolean targetFound = false;
	Gun gun;

	@Override
	public void init() {
		robot.debug.addDebugPaint(new AimingLeadPaint(this));
		gun = robot.getStrategy(Gun.class);
	}

	// @Override
	// public AimData getAimData(ScannedRobotData scan) {
	// Vector2 enemyScanPosition = scan.getPositionOfScannedRobot();
	// double speed = scan.getEvent().getVelocity();
	// if (speed == 0) {
	// return new AimData(enemyScanPosition, 1);
	// }
	// Vector2 directionVector = MathUtil.getVectorFromRadians(
	// scan.getEvent().getHeadingRadians()).nor();
	// int maxTimeElapseToShoot = 20;
	//
	// long scanAge = scan.getAge();
	// pointAEnemyRay = enemyScanPosition.copy().add(
	// directionVector.copy().mult(speed * scanAge));
	//
	// double minPower = 0.1;
	// double maxPower = 3.0;
	//
	// Vector2 myCurrentPosition = robot.getPosition();
	// Vector2 myDirectionVector = MathUtil.getVectorFromRadians(
	// robot.getHeadingRadians()).nor();
	// double mySpeed = robot.getVelocity();
	//
	// AimData data = new AimData(new Vector2(), 0);
	// targetFound = false;
	// boolean bulletRadiusCollided = false;
	// FIND_TARGET: for (double power = maxPower; power > minPower; power -=
	// 0.1) {
	// double bulletVelocity = 20 - 3 * power;
	// for (int time = 0; time < maxTimeElapseToShoot; time += 1) {
	// Vector2 movement = directionVector.copy().mult(
	// speed * (time + scanAge));
	// pointBEnemyRay = enemyScanPosition.copy().add(movement);
	// double bulletCircleRadius = (time - gun
	// .turnsToAlignOn(pointBEnemyRay)) * bulletVelocity;
	// Vector2 bulletCircleCentre = myCurrentPosition.copy().add(
	// myDirectionVector.copy().mult(mySpeed * time));
	//
	// if (MathUtil.doesRayIntercectsSphere(pointAEnemyRay,
	// pointBEnemyRay, bulletCircleCentre, bulletCircleRadius)) {
	// bulletRadiusCollided = true;
	// radius = bulletCircleRadius;
	// centre = bulletCircleCentre;
	// data.position = pointBEnemyRay;
	// data.power = power;
	// double distanceToA = pointAEnemyRay.copy()
	// .subtract(myCurrentPosition).length();
	// double distanceToB = pointBEnemyRay.copy()
	// .subtract(myCurrentPosition).length();
	// targetFound = true;
	// if (distanceToA > distanceToB) {
	// break FIND_TARGET;
	// }
	// } else if (bulletRadiusCollided) {
	// break FIND_TARGET;
	// }
	// }
	// }
	// return data;
	// }

	@Override
	public AimData getAimData(ScannedRobotData scan) {
		Vector2 enemyScanPosition = scan.getPositionOfScannedRobot();
		double speed = scan.getEvent().getVelocity();
		if (speed == 0) {
			return new AimData(enemyScanPosition, 1);
		}
		Vector2 directionVector = MathUtil.getVectorFromRadians(
				scan.getEvent().getHeadingRadians()).nor();
		int maxTimeElapseToShoot = 20;

		long scanAge = scan.getAge();
		pointAEnemyRay = enemyScanPosition.copy().add(
				directionVector.copy().mult(speed * scanAge));

		double minPower = 0.1;
		double maxPower = 3.0;

		Vector2 myCurrentPosition = robot.getPosition();
		Vector2 myDirectionVector = MathUtil.getVectorFromRadians(
				robot.getHeadingRadians()).nor();
		double mySpeed = robot.getVelocity();

		AimData data = new AimData(new Vector2(), 0);
		targetFound = false;
		boolean intercectionFound = false;
		FIND_TARGET: for (double power = maxPower; power > minPower; power -= 0.1) {
			double bulletVelocity = 20 - 3 * power;
			for (int time = 0; time < maxTimeElapseToShoot; time += 1) {
				Vector2 movement = directionVector.copy().mult(
						speed * (time + scanAge));
				pointBEnemyRay = enemyScanPosition.copy().add(movement);
				double bulletCircleRadius = (time - gun
						.turnsToAlignOn(pointBEnemyRay)) * bulletVelocity;
				Vector2 bulletCircleCentre = myCurrentPosition.copy().add(
						myDirectionVector.copy().mult(mySpeed * time));
				List<Vector2> collisionPoints = MathUtil
						.whereDoesRayIntercectsSphere(pointAEnemyRay,
								pointBEnemyRay, bulletCircleCentre,
								bulletCircleRadius);
				if (!collisionPoints.isEmpty()) {
					radius = bulletCircleRadius;
					centre = bulletCircleCentre;
					double distanceToB = pointBEnemyRay.copy()
							.subtract(myCurrentPosition).length();
					if (Math.abs(distanceToB - radius) < 1.5) {
						boolean intercects = false;
						for (Vector2 vector2 : collisionPoints) {
							if (intersects(pointAEnemyRay, pointBEnemyRay,
									vector2)) {
								intercects = true;
							}
						}
						if (intercects) {
							intercectionFound = true;
							data.position = pointBEnemyRay;
							data.power = power;
							targetFound = true;
							double distanceToA = pointAEnemyRay.copy()
									.subtract(myCurrentPosition).length();
							if (distanceToA > distanceToB) {
								break FIND_TARGET;
							}
						} else if (intercectionFound) {
							break FIND_TARGET;
						}
					}
				} else if (intercectionFound) {
					break FIND_TARGET;
				}
			}
		}
		return data;
	}

	public boolean intersects(Vector2 lineA, Vector2 lineB, Vector2 point) {
		double distanceToA = point.copy().subtract(lineA).length();
		double distanceToB = lineB.copy().subtract(point).length();
		double lineLength = lineB.copy().subtract(lineA).length();
		return (distanceToA + distanceToB) == lineLength;
	}

	public class AimingLeadPaint implements DebugCommand {

		AimingLead lead;

		public AimingLeadPaint(AimingLead lead) {
			super();
			this.lead = lead;
		}

		@Override
		public void execute(Graphics2D g) {
			if (lead.targetFound) {
				g.setColor(Color.cyan);
				g.drawLine((int) lead.pointAEnemyRay.x,
						(int) lead.pointAEnemyRay.y,
						(int) lead.pointBEnemyRay.x,
						(int) lead.pointBEnemyRay.y);
				g.setColor(Color.green);
				Vector2 centre = lead.centre;
				Double radius = lead.radius;
				g.drawOval((int) (centre.x - radius),
						(int) (centre.y - radius), (int) (radius * 2),
						(int) (radius * 2));
			}
		}

	}

}
