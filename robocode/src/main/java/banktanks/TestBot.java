package banktanks;

import banktanks.controllers.aiming.Aiming;
import banktanks.controllers.aiming.AimingLead;
import banktanks.controllers.movement.OrbitMovement;
import banktanks.controllers.targeting.TargetStrongest;
import banktanks.debug.DefaultDebug;
import banktanks.devices.Gun;
import banktanks.devices.ScannedRobotMemory;
import banktanks.devices.TomTom;
import banktanks.hulls.StrategyHull;

public class TestBot extends StrategyHull {

	public TestBot() {
		setDebug(new DefaultDebug(false));

		addStrategy(new TomTom());
		addStrategy(new ScannedRobotMemory());
		addStrategy(new Gun());
		
		
		
		addStrategy(new OrbitMovement());
		addStrategy(new TargetStrongest());
		addStrategy(new AimingLead(),Aiming.class);
	}
}