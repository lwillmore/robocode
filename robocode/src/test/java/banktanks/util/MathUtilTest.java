package banktanks.util;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class MathUtilTest {

	@Test
	public void getVectorFromRadians() {
		Vector2 expected = new Vector2(-1, 0);
		Vector2 actual = MathUtil.getVectorFromRadians(3 * Math.PI / 2);
		Assert.assertEquals(expected, actual);

		expected = new Vector2(0, -1);
		actual = MathUtil.getVectorFromRadians(Math.PI);
		Assert.assertEquals(expected, actual);

		expected = new Vector2(0, 1);
		actual = MathUtil.getVectorFromRadians(0);
		Assert.assertEquals(expected, actual);

	}

	@Test
	public void doesRayIntercectsSphere() {
		Vector2 pointA = new Vector2(0, 0);
		Vector2 pointB = new Vector2(0, 2);
		Vector2 centre = new Vector2(0, 1);
		double r = 0.1;

		boolean actual = MathUtil.doesRayIntercectsSphere(pointA, pointB,
				centre, r);
		boolean expected = true;

		Assert.assertEquals(expected, actual);

		centre = new Vector2(1, 1);

		actual = MathUtil.doesRayIntercectsSphere(pointA, pointB, centre, r);
		expected = false;

		Assert.assertEquals(expected, actual);

	}
	
	
	@Test
	public void wherDoesRayIntersectSphere(){
		Vector2 pointA = new Vector2(1, 10);
		Vector2 pointB = new Vector2(1, 8);
		Vector2 centre = new Vector2(0, 0);
		double r = 2;

		List<Vector2> actual = MathUtil.whereDoesRayIntercectsSphere(pointA, pointB, centre, r);
		Assert.assertTrue(actual.isEmpty());
		
		actual = MathUtil.whereDoesRayIntercectsSphere(pointA, pointB, centre, r);
		Assert.assertTrue(actual.size()==1);
		

	}

}
