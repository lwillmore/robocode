package banktanks.devices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import banktanks.util.Vector2;

public class TomTomTest {
	
	TomTom objectUnderTest;
	
	@Before
	public void setup(){
		objectUnderTest = new TomTom();
	}

	@Test
	public void driveTo(){
		Vector2 desiredDestination = new Vector2(1,2);
		objectUnderTest.driveTo(desiredDestination);
		assertTrue(objectUnderTest.route.size()==1);
		assertTrue(objectUnderTest.route.peek().equals(desiredDestination));
	}
	
	@Test
	public void driveRoute(){
		List<Vector2> routePlan = new ArrayList<Vector2>();
		routePlan.add(new Vector2(1,2));
		routePlan.add(new Vector2(1,3));
		routePlan.add(new Vector2(1,4));
		routePlan.add(new Vector2(1,5));
		objectUnderTest.driveRoute(routePlan);
		for (int i = 0; i < routePlan.size(); i++) {
			Vector2 nextInPlan = routePlan.get(i);
			Vector2 nextCurrentDestination = objectUnderTest.route.pop();
			assertEquals(nextInPlan,nextCurrentDestination);
		}
	}
}
