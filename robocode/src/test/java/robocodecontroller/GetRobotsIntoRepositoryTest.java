package robocodecontroller;

import org.junit.Test;

import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

public class GetRobotsIntoRepositoryTest {
	
	@Test
	public void getRobotsIntoRepository(){
		 RobocodeEngine engine = new RobocodeEngine(new java.io.File("C:/Robocode"));
		 
		 RobotSpecification[] robots = engine.getLocalRepository();
		 for (int i = 0; i < robots.length; i++) {
			RobotSpecification robotSpecification = robots[i];
			System.out.println(robotSpecification.getName());
		}
		 
	}

}
